package com.infullmobile.androidhomework.domain.model

data class WeatherForecast(
        val city: City
)
