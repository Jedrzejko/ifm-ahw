package com.infullmobile.androidhomework.presentation.weather.di

import com.infullmobile.androidhomework.domain.WeatherService
import com.infullmobile.androidhomework.repository.DemoWeatherService
import dagger.Module
import dagger.Provides

@Module
class WeatherServiceModule {

    @Provides
    internal fun providesWeatherService(): WeatherService = DemoWeatherService()
}
