package com.infullmobile.androidhomework.presentation.weather

import android.net.Uri
import android.os.Bundle
import com.infullmobile.android.infullmvp.Presenter

open class WeatherPresenter(
        private val model: WeatherModel,
        view: WeatherView
) : Presenter<WeatherView>(view) {

    override fun bind(intentBundle: Bundle, savedInstanceState: Bundle, intentData: Uri?) {
        model.getWeatherForecastForCity("warsaw")
                .subscribe { weatherForecast -> presentedView.displayForecast(weatherForecast) }
    }
}
