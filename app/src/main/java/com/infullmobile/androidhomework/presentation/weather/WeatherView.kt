package com.infullmobile.androidhomework.presentation.weather

import android.support.annotation.LayoutRes
import android.widget.TextView
import com.infullmobile.android.infullmvp.PresentedActivityView
import com.infullmobile.androidhomework.R
import com.infullmobile.androidhomework.domain.model.WeatherForecast

open class WeatherView : PresentedActivityView<WeatherPresenter>() {

    @LayoutRes override val layoutResId = R.layout.activity_weather
    val cityName: TextView by bindView(R.id.cityName)

    override fun onViewsBound() {
        // NO-OP
    }

    open fun displayForecast(weatherForecast: WeatherForecast) {
        cityName.text = weatherForecast.city.name
    }
}
