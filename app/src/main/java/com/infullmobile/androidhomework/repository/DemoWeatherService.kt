package com.infullmobile.androidhomework.repository

import com.infullmobile.androidhomework.domain.WeatherService
import com.infullmobile.androidhomework.domain.model.City
import com.infullmobile.androidhomework.domain.model.WeatherForecast
import io.reactivex.Single
import java.util.concurrent.TimeUnit

class DemoWeatherService : WeatherService {

    override fun getWeatherForecastForCity(cityName: String): Single<WeatherForecast> =
            Single.fromCallable { WeatherForecast(City("sample city name", "sample country name")) }
                    .delay(DELAY_TIME, DELAY_TIME_UNIT)

    companion object {
        private const val DELAY_TIME = 1000L
        private val DELAY_TIME_UNIT = TimeUnit.MILLISECONDS
    }
}
