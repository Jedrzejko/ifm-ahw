package com.infullmobile.androidhomework.domain.model

object WeatherForecastFactory {

    const val TEST_CITY_NAME = "Warsaw"
    const val TEST_CITY_COUNTRY = "Poland"
    val TEST_WEATHER_CITY = City(TEST_CITY_NAME, TEST_CITY_COUNTRY)

    fun createTestWeatherForecast() = WeatherForecast(TEST_WEATHER_CITY)
}